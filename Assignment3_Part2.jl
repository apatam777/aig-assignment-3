### A Pluto.jl notebook ###
# v0.14.4

using Markdown
using InteractiveUtils

# ╔═╡ 6588e7d8-51bd-4e93-80ab-80ad2e607f83
using Pkg; Pkg.add("PlutoUI");

# ╔═╡ cc0c32b4-1d6a-4bbd-ae89-42b3abe5db27
using PlutoUI

# ╔═╡ ce9fc364-458f-4eb9-9446-298726fe57c4
using DataStructures

# ╔═╡ d2b0d048-d335-460b-8bba-901b63722fac
md"### Declaring the player"

# ╔═╡ 89d97847-1fb0-4219-af09-9cdc82a4c61b
p1 = ([1,4], [2, 3])

# ╔═╡ b195bc87-b51f-49d5-be73-13ab6d9d0577
p2 = ([2,0], [1, 2])

# ╔═╡ eaaa86bc-d3ba-4288-9707-b6b9bd52c146
game = (p1,p2)

# ╔═╡ f8e521f0-b2e1-4577-95cf-1a423adbb3a8
md"### Variables to store player matrices"

# ╔═╡ 7e9491bd-ea04-4425-8470-42c361d794fc
a = []

# ╔═╡ b7f6795b-d34e-4133-9367-db54ed652830
b = []

# ╔═╡ 03c3722d-42e3-4e48-9107-ead3babf799c
c = []

# ╔═╡ d6dbca9c-3db9-4117-ba4c-825d9acd3836
d = []

# ╔═╡ 1c0824ce-f69e-4f3e-ae2a-0fc48f2bb953
ind = 1

# ╔═╡ 7bf1299c-32a1-4cf0-a79a-54d8cb37e7d3
md"### Function to populate arrays where player matrices stored"

# ╔═╡ c9cdfaae-cbfc-439d-9b4f-d836a7397395
function obtainVal(player, index, a1, a2)
	
	for i in player[index]	
		push!(a1, player[index][1])	
		push!(b2, player[index][2])
		index+=1
	end
end

# ╔═╡ 7bda29a4-7f9c-444a-9e09-981c5d03f770
for c in p1[ind]	
	push!(a, p1[ind][1])	
	push!(b, p1[ind][2])
	ind+=1
	
end

# ╔═╡ 6821bf1c-90f1-494d-8e88-0392548d025e
for i in p2[ind]	
	push!(c, p2[ind][1])	
	push!(d, p2[ind][2])
	ind+=1
	
end

# ╔═╡ c424e5dc-aa80-443e-9fca-283ddd28d31a
md"### View Stored Values"

# ╔═╡ 64091ca8-dd44-478d-a4e7-dd18c15f209f
a

# ╔═╡ 4dbc8844-99b3-468b-b36d-ed27e7b8ce95
b

# ╔═╡ 61bb4870-3a08-49cd-9346-6cde3816d13a
c

# ╔═╡ 07cb16d4-32d1-4c35-9d50-1c98e56f6313
d

# ╔═╡ b11abb94-2064-4abe-8632-d8dd867bed89
md"#### Player 1 Pure Strategy Payoffs"

# ╔═╡ 1d197d73-25bf-4cde-bfa1-a881aaf58255
v1 = a[1]

# ╔═╡ c233ae3f-762e-41f2-9781-cec447553106
v2 = b[1]

# ╔═╡ 4d02b4c6-6710-4232-9cd0-777b5fa0e93b
v3 = a[2]

# ╔═╡ 94dc88ff-f052-4b95-9619-26cf14eec828
v4 = b[2]

# ╔═╡ 136b9bbb-7aae-4502-b509-b3ab9732585a
md"#### Player 2 Pure Strategy Payoffs"

# ╔═╡ ab0d666d-6570-45e7-a73b-4443f8e13d92
w1 = c[1]

# ╔═╡ 4ffe8467-0222-4fc2-a20a-1ff58ba32420
w2 = d[1]

# ╔═╡ c0887531-bcfe-488f-8d14-74916548e290
w3 = c[2]

# ╔═╡ ab8edb0a-eaa0-4400-8670-c8322fdae9fb
w4 = d[2]

# ╔═╡ 741bfa31-b06e-435d-bc57-1d6bb8bb335c
md"### Calculate Payoff Matrix"

# ╔═╡ 6fecdd05-f79b-4a3b-a9fe-0c5daf5b3533
function payoff_Matrix(v1, v2, v3, v4)

	ans = 0.001
	ans1 = 0.001
	
	while ans <= 1 && ans1 >-1	
		
		sol1 = (v1*ans) + (1 - ans)*v2
		sol2 = (v3*ans) + (1 - ans)*v4	
		
		sol3 = (v1*ans1) + (1 - ans1)*v2
		sol4 = (v3*ans1) + (1 - ans1)*v4	
	
		if sol1 < (sol2 + 0.0001) && sol1 > (sol2 - 0.0001)			
			return ans
			break
			
		elseif sol3 < (sol4 + 0.00001) && sol3 > (sol4 - 0.00001)
			return ans1
			break
	
		else 
			ans += 0.00001
			ans1 -= 0.00001
			
		end
	end
end

# ╔═╡ a08f7675-b1dd-4dd4-881b-aba2aa3db2f0
payoff1 = payoff_Matrix(v1, v2, v3, v4)

# ╔═╡ a740ae7c-6411-4b68-b6ca-d08ea1c74786
payoff2 = payoff_Matrix(w1, w2, w3, w4)

# ╔═╡ f6821b92-e317-4932-bd8a-bb3954304151
md"### Calculate Payoff For Each Player"

# ╔═╡ 9931d34b-41c8-4c9d-a19f-912dd85c1903
function calculate_Payoff_Player1(pay1, pay2, p1)
	r1c1 = pay1*pay2
	r1c2 = pay1*pay1
	r2c1 = pay2*pay2
	r2c2 = pay2*pay1
	
	payoff_Player1 = (p1[1][1]*r1c1) + (p1[1][2]*r1c2)+ (p1[2][1]*r2c1) + (p1[2][2]*r2c2)
	
	return payoff_Player1		
end

# ╔═╡ 59a79e9d-1b49-4d99-83a7-b111e835a167
calculate_Payoff_Player1(payoff1, payoff2, p1)

# ╔═╡ 7595b810-fba9-4568-ae3a-293f51046c71
function calculate_Payoff_Player2(pay1, pay2, p2)
	r1c1 = pay1*pay2
	r1c2 = pay1*pay1
	r2c1 = pay2*pay2
	r2c2 = pay2*pay1
	
	payoff_Player2 = (p2[1][1]*r1c1) + (p2[1][2]*r1c2)+ (p2[2][1]*r2c1) + (p2[2][2]*r2c2)	
	
	return payoff_Player2
end

# ╔═╡ ef8dc29b-9583-4fa3-8bd7-198de1881604
calculate_Payoff_Player2(payoff1, payoff2, p2)

# ╔═╡ Cell order:
# ╠═6588e7d8-51bd-4e93-80ab-80ad2e607f83
# ╠═cc0c32b4-1d6a-4bbd-ae89-42b3abe5db27
# ╠═ce9fc364-458f-4eb9-9446-298726fe57c4
# ╟─d2b0d048-d335-460b-8bba-901b63722fac
# ╠═89d97847-1fb0-4219-af09-9cdc82a4c61b
# ╠═b195bc87-b51f-49d5-be73-13ab6d9d0577
# ╠═eaaa86bc-d3ba-4288-9707-b6b9bd52c146
# ╟─f8e521f0-b2e1-4577-95cf-1a423adbb3a8
# ╠═7e9491bd-ea04-4425-8470-42c361d794fc
# ╠═b7f6795b-d34e-4133-9367-db54ed652830
# ╠═03c3722d-42e3-4e48-9107-ead3babf799c
# ╠═d6dbca9c-3db9-4117-ba4c-825d9acd3836
# ╠═1c0824ce-f69e-4f3e-ae2a-0fc48f2bb953
# ╟─7bf1299c-32a1-4cf0-a79a-54d8cb37e7d3
# ╠═c9cdfaae-cbfc-439d-9b4f-d836a7397395
# ╠═7bda29a4-7f9c-444a-9e09-981c5d03f770
# ╠═6821bf1c-90f1-494d-8e88-0392548d025e
# ╟─c424e5dc-aa80-443e-9fca-283ddd28d31a
# ╠═64091ca8-dd44-478d-a4e7-dd18c15f209f
# ╠═4dbc8844-99b3-468b-b36d-ed27e7b8ce95
# ╠═61bb4870-3a08-49cd-9346-6cde3816d13a
# ╠═07cb16d4-32d1-4c35-9d50-1c98e56f6313
# ╟─b11abb94-2064-4abe-8632-d8dd867bed89
# ╠═1d197d73-25bf-4cde-bfa1-a881aaf58255
# ╠═c233ae3f-762e-41f2-9781-cec447553106
# ╠═4d02b4c6-6710-4232-9cd0-777b5fa0e93b
# ╠═94dc88ff-f052-4b95-9619-26cf14eec828
# ╟─136b9bbb-7aae-4502-b509-b3ab9732585a
# ╠═ab0d666d-6570-45e7-a73b-4443f8e13d92
# ╠═4ffe8467-0222-4fc2-a20a-1ff58ba32420
# ╠═c0887531-bcfe-488f-8d14-74916548e290
# ╠═ab8edb0a-eaa0-4400-8670-c8322fdae9fb
# ╟─741bfa31-b06e-435d-bc57-1d6bb8bb335c
# ╠═6fecdd05-f79b-4a3b-a9fe-0c5daf5b3533
# ╠═a08f7675-b1dd-4dd4-881b-aba2aa3db2f0
# ╠═a740ae7c-6411-4b68-b6ca-d08ea1c74786
# ╟─f6821b92-e317-4932-bd8a-bb3954304151
# ╠═9931d34b-41c8-4c9d-a19f-912dd85c1903
# ╠═59a79e9d-1b49-4d99-83a7-b111e835a167
# ╠═7595b810-fba9-4568-ae3a-293f51046c71
# ╠═ef8dc29b-9583-4fa3-8bd7-198de1881604
