### A Pluto.jl notebook ###
# v0.14.4

using Markdown
using InteractiveUtils

# ╔═╡ 258abe6c-bc04-4349-be30-c91b162ad107
using Pkg; Pkg.add("PlutoUI"); 

# ╔═╡ 420014f6-392b-4cd9-b0e7-99d6f5d05041
using PlutoUI

# ╔═╡ 654b807e-eb9d-4c66-83b4-63143c349d9a
using DataStructures

# ╔═╡ 562aade3-08a4-4732-b08e-87a853645555
threshold = 0.001

# ╔═╡ 27778e0f-9b70-4ada-9fb8-5b493825b540
#discount
Gamma = 0.9

# ╔═╡ f851bf31-661b-4ece-80bd-7d340e73c35c
Noise = 0.1

# ╔═╡ 5a7b31fa-596a-43a9-a309-26af962058b3
states = []

# ╔═╡ 297817d9-da6a-4559-a532-18826ec47b73
pos_act = ["Up", "Down", "Left", "Right"]

# ╔═╡ de791665-d54a-402e-ad81-df585d5f4900
row = 3

# ╔═╡ d99ba3a7-5159-4c07-beab-8780e967af15
column = 3

# ╔═╡ a327ab8e-b61a-4310-990b-82c22265d4c2
md"### Populate for States"

# ╔═╡ 8e9f0f57-6a9f-49be-8cb0-6b044c7fac94
function populate_States(row, column, states)
	for i in 0:(row-1)
		for j in 0:(column-1)
			val = [i,j]
			push!(states, val)
		end
	end
end

# ╔═╡ 8cb4de3f-53c8-48ff-bd47-6b577a218cb7
populate_States(row, column, states)

# ╔═╡ 8dc260d0-3d70-457e-ac9d-d570f5efc592
states

# ╔═╡ 2774b280-6209-43b0-9c2a-ee8126150f8b
md"#### Keys Needed to Access Dictonary"

# ╔═╡ d9fdf94c-185b-4572-b4f4-6ab4b359ed46
keys = []

# ╔═╡ 61cc7356-9e4e-4ddf-9ee8-bdfbf5a918a1
function access_key(states, keys)
	for i in 1:length(states)	
		k = (states[i][1], states[i][2])	
		push!(keys, k)
	end
end

# ╔═╡ 7bc2e7a1-3daf-4226-8b75-9ab3abe325ca
access_key(states, keys)

# ╔═╡ 2f110630-bec3-46bc-bea5-7c1e81de216a
keys

# ╔═╡ 8c76508c-cc60-4682-836a-cf4eb72acb50
md"#### Actions"

# ╔═╡ 87fe33d9-028a-4242-8016-a37c9472a616
act = Dict()

# ╔═╡ adae5f33-6a0b-4217-b04b-3a45145ad5e3
push!(act, (0,0) => ("Up", "Right"))

# ╔═╡ 1b0bbac0-ab24-4890-9ffa-98b026e6cfec
push!(act, (0,1) => ("Up", "Right", "Down"))

# ╔═╡ 2ad9ab31-19e6-48d3-a5a4-1e17e7ea7807
push!(act, (0,2) => ("Right", "Down"))

# ╔═╡ 9dc84b3d-d046-4bdc-a1bb-a5dfb6de3bbf
push!(act, (1,0) => ("Up", "Right", "Left"))

# ╔═╡ 6adcf0c5-da5c-4a77-b831-17dda2f8275b
push!(act, (1,1) => ("Up", "Right", "Down", "Left"))

# ╔═╡ 4a26d320-e84e-4137-922a-84521d45db8b
push!(act, (1,2) => ("Right", "Down", "Left"))

# ╔═╡ 191f9924-055b-4a8f-ba20-95f143a3b171
push!(act, (2,0) => ("Up", "Left"))

# ╔═╡ 2a394e42-b601-4c04-8781-971c804057bd
push!(act, (2,1) => ("Up", "Down", "Left"))

# ╔═╡ 12816f86-7f84-4bc6-a0e2-629600e7ce4f
push!(act, (2,2) => ("Down", "Left"))

# ╔═╡ 3abce016-2d5e-43ab-9cb7-fc2e1b42ceda
md"#### Functions to Obtain Transition Model"

# ╔═╡ 41d3a08a-eefa-477a-9963-c2d80701fa37
function trans_Model1(pos_act, act, transmodel, keys)
	for i in 1:length(pos_act)	
		for s in keys
			if pos_act[i] in act[s]
			
				if pos_act[i] == "Up"
					nxt = [s[1], s[2]+1]
					push!(transmodel, s => ("Up", nxt))
				end
			end
		end
	end
end

# ╔═╡ 7ca9f68d-3997-4393-bb00-5596cb910a56
function trans_Model2(pos_act, act, transmodel, keys)
	for i in 1:length(pos_act)	
		for s in keys
			if pos_act[i] in act[s]
			
				if pos_act[i] == "Down"
					nxt = [s[1], s[2]-1]
					push!(transmodel, s => ("Down", nxt))
				end
			end
		end
	end
end

# ╔═╡ 04208508-02f6-49ff-88dd-0f853ac40e06
function trans_Model3(pos_act, act, transmodel, keys)
	for i in 1:length(pos_act)	
		for s in keys
			if pos_act[i] in act[s]
			
				if pos_act[i] == "Left"
					nxt = [s[1]-1, s[2]]
					push!(transmodel, s => ("Left", nxt))
				end
			end
		end
	end
end

# ╔═╡ 761164f7-680b-4767-a6a3-224f0885af5c
function trans_Model4(pos_act, act, transmodel, keys)
	for i in 1:length(pos_act)	
		for s in keys
			if pos_act[i] in act[s]
			
				if pos_act[i] == "Right"
					nxt = [s[1]+1, s[2]]
					push!(transmodel, s => ("Right", nxt))
				end
			end
		end
	end
end

# ╔═╡ 5a42542d-a05a-4af4-aab4-edadf58fd084
transMod1 = Dict()

# ╔═╡ 31bc9059-b73b-4277-9293-f8f8b9673218
transMod2 = Dict()

# ╔═╡ 31b79bd7-6066-432e-8487-970bccc4f8f2
transMod3 = Dict()

# ╔═╡ c8e82812-10fb-4922-8e61-9a4c9d2f4de7
transMod4 = Dict()

# ╔═╡ 8fa45769-d2ae-4161-b6ca-fbdec6869d92
trans_Model1(pos_act, act, transMod1, keys)

# ╔═╡ 15985a01-b3e8-4ac1-84e6-abddd11f29d8
trans_Model2(pos_act, act, transMod2, keys)

# ╔═╡ e76630e6-bef2-41a8-a168-87facb62d5fc
trans_Model3(pos_act, act, transMod3, keys)

# ╔═╡ cdf413d5-95f8-41ae-b791-3adaf0721902
trans_Model4(pos_act, act, transMod4, keys)

# ╔═╡ 2cd58b6b-c850-43a0-88b9-d1e2ebce5ce1
transMod1

# ╔═╡ 8b6e63a1-fe8d-4817-9a41-674fdc8e4af2
transMod2

# ╔═╡ b5119288-9152-474d-80b5-7626df1c5352
transMod3

# ╔═╡ 13c801c4-5c6c-4037-b1a7-536ab2d7c2f5
transMod4

# ╔═╡ 497b3c47-6f51-470d-88ea-0235053cd6bd
md"#### Reward Function"

# ╔═╡ 19b18116-beda-43b3-8041-4ad6c8360ca3
reward = Dict()

# ╔═╡ 68c75240-ae38-4050-bf05-9272c5ad9798
function reward_Function(key, reward)
	for i in key
    	if i == [0,2]
       		push!(reward, (i[1], i[2]) => -1)	
		
		elseif i == [1, 1]
	  		push!(reward, (i[1], i[2]) => 1)
		
		elseif i == [2, 1]
	  		push!(reward, (i[1], i[2]) => -1)
		
		elseif i == [0, 1]
	  		push!(reward, (i[1], i[2]) => 1)
		
		elseif i == [1, 2]
	  		push!(reward, (i[1], i[2]) => -1)	
	
		elseif i == [2, 1]
	  		push!(reward, (i[1], i[2]) => 1)	
		
		else
			push!(reward, (i[1], i[2]) => 0)
		end
	end
end

# ╔═╡ 9195378f-c8af-4fd5-aebd-f4e8e3dab98a
reward_Function(states, reward)

# ╔═╡ 1203cd4d-2525-4951-b3ed-3cb74e0b9330
reward

# ╔═╡ a93fd28a-5436-4b9c-9c0d-75e9eb4fb88b
md"### Policy"

# ╔═╡ 6399ec65-6f63-4d02-9f42-c658dd8b1e91
initial = (0,0)

# ╔═╡ f32f6ede-5827-4158-a1d2-099a64299767
pol = Dict()

# ╔═╡ bdce9995-158f-49b2-88fe-7531e058cd8f
md"##### Function to Obtain Policy"

# ╔═╡ 4a52cd50-9cb3-4323-8da5-772589734216
function set_Pol(transMod1, transMod2, transMod3, transMod4, initial, goal, pol, act)
	n = initial
	
		while (n != goal) == true		
			p = rand(act[n])		
		
			if p == "Up"
				push!(pol, n => transMod1[n])
				if (n != goal) == false
					break
				
				else
				a = (transMod1[n][2][1], transMod1[n][2][2])
				n = a
				end
		
			elseif p == "Down"
				push!(pol, n => transMod2[n])
				if (n != goal) == false
					break
				
				else
				a = (transMod2[n][2][1], transMod2[n][2][2])
				n = a	
				end
				
			elseif p == "Left"
				push!(pol, n => transMod3[n])
				if (n != goal) == false
					break
				
				else
				a = (transMod3[n][2][1], transMod3[n][2][2])
				n = a
				end
		
			elseif p == "Right"
				push!(pol, n => transMod4[n])
				if (n != goal) == false
					break
				else
				a = (transMod4[n][2][1], transMod4[n][2][2])
				n = a
				end
			end
		end		
	
end

# ╔═╡ f31d00bd-0930-40b9-b449-679723775b96
goal = (1,2)

# ╔═╡ f83980d5-6f0d-4cae-a76b-f79a99f50045
set_Pol(transMod1, transMod2, transMod3, transMod4, initial, goal, pol, act)

# ╔═╡ 613f6e21-c29c-40b2-8540-71bcc2ef0716
pol

# ╔═╡ 86dd4001-2161-413e-9594-074781a0b7e4
policy = []

# ╔═╡ c3a77ee6-999c-4565-98dd-a25c90e86283
md"##### Actions Within Policy"

# ╔═╡ c7d0c0e0-d328-4559-bbdc-070a73dc1ffe
function get_Initial_Policy(keys, pol, policy)
	for k in keys
		if haskey(pol, k) == true
		push!(policy, pol[k][1])
		end
	end
end	

# ╔═╡ 3602804b-e80f-4308-8c9d-c22b54032837
get_Initial_Policy(keys, pol, policy)

# ╔═╡ 3daba2be-1989-4031-88a3-efcd92cdf112
policy

# ╔═╡ 2dc2bc19-ee82-44d3-bb32-c476bc8a5462
md"### Probability of Taking certain action"

# ╔═╡ d4f4b321-d005-4271-a1d7-62713ac7412d
action_Prob = Dict()

# ╔═╡ 7bc2ddbe-c87f-469e-997f-3680effddfdb
function act_Prob(keys, pol, act, action_Prob)	
	for k in keys
		if haskey(pol, k)			 
			prob = 1/length(act[k])
			push!(action_Prob, k => prob)
		end
	end
end				

# ╔═╡ 6744723c-19e5-403e-89d5-85c4332000e3
act_Prob(keys, pol, act, action_Prob)

# ╔═╡ 75bc728d-1ceb-42e5-aeaa-4924ff9f85d0
action_Prob

# ╔═╡ 9ab6a9b3-f310-45c1-ad22-b96e9e61391f
md"#### Populate State Values"

# ╔═╡ 95644daa-fdca-4b2e-a598-9b2ac1a07499
state_Val = Dict()

# ╔═╡ 155eaece-46ea-4b77-8f9f-47e72fb15f7c
function populate_state_Values(pol, keys,state_Val)
	for i in keys
		sv = 0
		if haskey(pol, i)
		push!(state_Val, i => sv)
		end
	end
end

# ╔═╡ 35f3f3e1-7f6a-4dea-9834-e259c2951f8e
populate_state_Values(pol, keys,state_Val)

# ╔═╡ 41318d34-64a1-4778-8cd4-85ad01bc14e2
state_Val

# ╔═╡ 8d357189-d0f0-443d-b9a8-4364547fe08e
md"#### Evaluate Policy"

# ╔═╡ 5249c9fa-3b36-42fb-84f4-5be221074882
function Policy_Evaluation(keys, pol, gamma, reward, action_Prob, state_Val, threshold)	
	current_values = []
	previous_values = []
	cur_Val = 0
	pre_Val = 0	
	index = 0
	total = 10
	while (total > threshold)	
		for s in keys
			if haskey(pol, s) == true			
				nxt = (pol[s][2][1], pol[s][2][2])
				next_r = reward[nxt]
				if haskey(pol, nxt)				
					state_V = action_Prob[s] * (next_r + gamma * state_Val[nxt]) 
					append!(current_values, state_V)
					append!(previous_values, state_Val[s])
				
				else				
					state_V = action_Prob[s] * (next_r + gamma * state_Val[s]) 
					append!(current_values, state_V)
					append!(previous_values, state_Val[s])
				end
			end
		end
		empty!(state_Val)
	
		for s in keys
			if haskey(pol, s) == true	
				
				index+=1			
				push!(state_Val, s => current_values[index])
				cur_Val = cur_Val + current_values[index]
				pre_Val = pre_Val + previous_values[index]	
				
			end
		end
		total = abs(cur_Val - pre_Val)
		empty!(current_values)
		empty!(previous_values)
	end
		
		return state_Val
	
end	

# ╔═╡ 8ac133a5-9e94-44b9-b74c-fcfc9b11f0af
Policy_Evaluation(keys, pol, Gamma, reward, action_Prob, state_Val, threshold)

# ╔═╡ 25ae5cd8-4af9-4dc4-933b-4fb7a32f0837
function Policy_Improvement(keys, pol, gamma, reward, action_Prob, state_Val, threshold, act, goal, transMod1,transMod2,transMod3, transMod4)
	
	total_Original_Value = 0
	new_Policy = pol	
	current_Optimal_Policy = pol
	
	p = Policy_Evaluation(keys, pol, gamma, reward, action_Prob, state_Val, threshold)
	
	for i in keys
		n = i
		if haskey(p, i)
			total_Original_Value += p[n]
			
			for a in 1:length(act[n])
				action = act[n]
				
				if action == "Up"
					pop!(new_Policy, n)
					push!(new_Policy, n => transMod1[n])
					if (n != goal) == false
						break
				
					else
						next_Act = (transMod1[n][2][1], transMod1[n][2][2])
						n = next_Act
					end
		
				elseif p == "Down"
					pop!(new_Policy, n)
					push!(new_Policy, n => transMod2[n])
					if (n != goal) == false
						break
				
					else
						next_Act = (transMod2[n][2][1], transMod2[n][2][2])
						n = next_Act	
					end
				
				elseif p == "Left"
					pop!(new_Policy, n)
					push!(new_Policy, n => transMod3[n])
					if (n != goal) == false
						break
				
					else
						a = (transMod3[n][2][1], transMod3[n][2][2])
						n = a
					end
		
				elseif p == "Right"
					pop!(new_Policy, n)
					push!(new_Policy, n => transMod4[n])
					if (n != goal) == false
						break
					else
					a = (transMod4[n][2][1], transMod4[n][2][2])
					n = a
					end
				end
				act_prob = act_Prob(keys, new_Policy, act, action_Prob)
				q = Policy_Evaluation(keys, new_Policy, gamma, reward, act_prob, state_Val, threshold)
				
				total_Original_Value 
				potential_Value = total_Original_Value - q[n]
				
				if potential_Value > total_Original_Value
					current_Optimal_Policy = new_Policy
					action_Prob = act_prob
				end			
				
			end
		end
	end
	return current_Optimal_Policy
end	

# ╔═╡ 35c214b1-1687-4d86-804b-dec2709db58a
#Policy_Improvement(keys, pol, Gamma, reward, action_Prob, state_Val, threshold, act, goal, transMod1,transMod2,transMod3, transMod4)

# ╔═╡ Cell order:
# ╠═258abe6c-bc04-4349-be30-c91b162ad107
# ╠═420014f6-392b-4cd9-b0e7-99d6f5d05041
# ╠═654b807e-eb9d-4c66-83b4-63143c349d9a
# ╠═562aade3-08a4-4732-b08e-87a853645555
# ╠═27778e0f-9b70-4ada-9fb8-5b493825b540
# ╠═f851bf31-661b-4ece-80bd-7d340e73c35c
# ╠═5a7b31fa-596a-43a9-a309-26af962058b3
# ╠═297817d9-da6a-4559-a532-18826ec47b73
# ╠═de791665-d54a-402e-ad81-df585d5f4900
# ╠═d99ba3a7-5159-4c07-beab-8780e967af15
# ╟─a327ab8e-b61a-4310-990b-82c22265d4c2
# ╠═8e9f0f57-6a9f-49be-8cb0-6b044c7fac94
# ╠═8cb4de3f-53c8-48ff-bd47-6b577a218cb7
# ╠═8dc260d0-3d70-457e-ac9d-d570f5efc592
# ╟─2774b280-6209-43b0-9c2a-ee8126150f8b
# ╠═d9fdf94c-185b-4572-b4f4-6ab4b359ed46
# ╠═61cc7356-9e4e-4ddf-9ee8-bdfbf5a918a1
# ╠═7bc2e7a1-3daf-4226-8b75-9ab3abe325ca
# ╠═2f110630-bec3-46bc-bea5-7c1e81de216a
# ╟─8c76508c-cc60-4682-836a-cf4eb72acb50
# ╠═87fe33d9-028a-4242-8016-a37c9472a616
# ╠═adae5f33-6a0b-4217-b04b-3a45145ad5e3
# ╠═1b0bbac0-ab24-4890-9ffa-98b026e6cfec
# ╠═2ad9ab31-19e6-48d3-a5a4-1e17e7ea7807
# ╠═9dc84b3d-d046-4bdc-a1bb-a5dfb6de3bbf
# ╠═6adcf0c5-da5c-4a77-b831-17dda2f8275b
# ╠═4a26d320-e84e-4137-922a-84521d45db8b
# ╠═191f9924-055b-4a8f-ba20-95f143a3b171
# ╠═2a394e42-b601-4c04-8781-971c804057bd
# ╠═12816f86-7f84-4bc6-a0e2-629600e7ce4f
# ╟─3abce016-2d5e-43ab-9cb7-fc2e1b42ceda
# ╠═41d3a08a-eefa-477a-9963-c2d80701fa37
# ╠═7ca9f68d-3997-4393-bb00-5596cb910a56
# ╠═04208508-02f6-49ff-88dd-0f853ac40e06
# ╠═761164f7-680b-4767-a6a3-224f0885af5c
# ╠═5a42542d-a05a-4af4-aab4-edadf58fd084
# ╠═31bc9059-b73b-4277-9293-f8f8b9673218
# ╠═31b79bd7-6066-432e-8487-970bccc4f8f2
# ╠═c8e82812-10fb-4922-8e61-9a4c9d2f4de7
# ╠═8fa45769-d2ae-4161-b6ca-fbdec6869d92
# ╠═15985a01-b3e8-4ac1-84e6-abddd11f29d8
# ╠═e76630e6-bef2-41a8-a168-87facb62d5fc
# ╠═cdf413d5-95f8-41ae-b791-3adaf0721902
# ╠═2cd58b6b-c850-43a0-88b9-d1e2ebce5ce1
# ╠═8b6e63a1-fe8d-4817-9a41-674fdc8e4af2
# ╠═b5119288-9152-474d-80b5-7626df1c5352
# ╠═13c801c4-5c6c-4037-b1a7-536ab2d7c2f5
# ╟─497b3c47-6f51-470d-88ea-0235053cd6bd
# ╠═19b18116-beda-43b3-8041-4ad6c8360ca3
# ╠═68c75240-ae38-4050-bf05-9272c5ad9798
# ╠═9195378f-c8af-4fd5-aebd-f4e8e3dab98a
# ╠═1203cd4d-2525-4951-b3ed-3cb74e0b9330
# ╟─a93fd28a-5436-4b9c-9c0d-75e9eb4fb88b
# ╠═6399ec65-6f63-4d02-9f42-c658dd8b1e91
# ╠═f32f6ede-5827-4158-a1d2-099a64299767
# ╟─bdce9995-158f-49b2-88fe-7531e058cd8f
# ╠═4a52cd50-9cb3-4323-8da5-772589734216
# ╠═f31d00bd-0930-40b9-b449-679723775b96
# ╠═f83980d5-6f0d-4cae-a76b-f79a99f50045
# ╠═613f6e21-c29c-40b2-8540-71bcc2ef0716
# ╠═86dd4001-2161-413e-9594-074781a0b7e4
# ╟─c3a77ee6-999c-4565-98dd-a25c90e86283
# ╠═c7d0c0e0-d328-4559-bbdc-070a73dc1ffe
# ╠═3602804b-e80f-4308-8c9d-c22b54032837
# ╠═3daba2be-1989-4031-88a3-efcd92cdf112
# ╟─2dc2bc19-ee82-44d3-bb32-c476bc8a5462
# ╠═d4f4b321-d005-4271-a1d7-62713ac7412d
# ╠═7bc2ddbe-c87f-469e-997f-3680effddfdb
# ╠═6744723c-19e5-403e-89d5-85c4332000e3
# ╠═75bc728d-1ceb-42e5-aeaa-4924ff9f85d0
# ╟─9ab6a9b3-f310-45c1-ad22-b96e9e61391f
# ╠═95644daa-fdca-4b2e-a598-9b2ac1a07499
# ╠═155eaece-46ea-4b77-8f9f-47e72fb15f7c
# ╠═35f3f3e1-7f6a-4dea-9834-e259c2951f8e
# ╠═41318d34-64a1-4778-8cd4-85ad01bc14e2
# ╟─8d357189-d0f0-443d-b9a8-4364547fe08e
# ╠═5249c9fa-3b36-42fb-84f4-5be221074882
# ╠═8ac133a5-9e94-44b9-b74c-fcfc9b11f0af
# ╠═25ae5cd8-4af9-4dc4-933b-4fb7a32f0837
# ╠═35c214b1-1687-4d86-804b-dec2709db58a
